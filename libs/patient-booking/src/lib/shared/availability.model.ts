export interface Availability {
  _id: string;
  eventIds: any[];
  motiveIds: string[];
  calendarId: string;
  nextAvailabilityId: string;
  previousAvailabilityId: string;
  appointmentSlotId: string;
  meridiem: string;
  state: string;
  duration: number;
  endAt: Date;
  startAt: Date;
  dayOfTheWeek: number;
  year: number;
  month: number;
  day: number;
  createdAt: Date;
  updatedAt: Date;
  __v: number;
  id: string;
}


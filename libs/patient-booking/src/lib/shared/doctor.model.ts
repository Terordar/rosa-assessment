export interface NewPatient {
  duration: number;
  isOnlineBookingAllowed: boolean;
}

export interface ExistingPatient {
  duration: number;
  isOnlineBookingAllowed: boolean;
}

export interface DefaultDurations {
  newPatient: NewPatient;
  existingPatient: ExistingPatient;
}

export interface CalendarConfiguration {
  category: string;
  status: string;
  defaultDurations: DefaultDurations;
  calendarId: string;
}

export interface Motive {
  id: string;
  label: string;
  allowedLocation: string;
  calendarConfigurations: CalendarConfiguration[];
  status: string;
}

export interface Coords {
  lat: number;
  lng: number;
}

export interface Address {
  _id: string;
  street: string;
  number: string;
  zipCode: string;
  city: string;
  country: string;
  coords: Coords;
}

export interface ContactInfo {
  isVerified: boolean;
  isEmergency: boolean;
  isPrimary: boolean;
  value: string;
  type: string;
}

export interface CarParks {
  isDisplayed: boolean;
  values: any[];
}

export interface Value {
  type: string;
  info: string;
}

export interface PublicTransports {
  isDisplayed: boolean;
  values: Value[];
}

export interface PracticalInformation {
  isWheelChairAccessible: boolean;
  carParks: CarParks;
  publicTransports: PublicTransports;
}

export interface Site {
  id: string;
  name: string;
  address: Address;
  contactInfos: ContactInfo[];
  organizationId: string;
  practicalInformation: PracticalInformation;
  businessHours: any[];
}

export interface Calendar {
  id: string;
  siteId: string;
  ownerId: string;
}

export interface BookingPreferences {
  bookingCancellationInSeconds: number;
  bookingNoticeInSeconds: number;
  bookingHorizonInSeconds: number;
  allowPatientsToLeaveNoteOnTakingAppointment: boolean;
}

export interface TemporaryMessage {
  title: string;
  content: string;
}

export interface Doctor {
  id: string;
  key: string;
  title: string;
  nihii: string;
  firstName: string;
  lastName: string;
  language: string;
  motives: Motive[];
  sites: Site[];
  calendars: Calendar[];
  bookingPreferences: BookingPreferences;
  legalGender: string;
  profilePictureUrl: string;
  about: string;
  allowedPaymentTypes: any[];
  website: string;
  temporaryMessage: TemporaryMessage;
  spokenLanguages: any[];
  trainings: any[];
  specialty?: any;
  specializations: any[];
}

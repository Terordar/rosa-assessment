import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Doctor } from '../shared/doctor.model';
import { Availability } from '../shared/availability.model';

interface GetAppointmentsParams {
  from: string;
  to: string;
  motiveId: string;
  isNewPatient: boolean;
  calendarId: string;
  state: string;
}

@Injectable({
  providedIn: 'root'
})
export class BookingService {
  constructor(private http: HttpClient) { }

  getDoctorByName(name: string) {
    return this.http.get<Doctor>(`https://staging-api.rosa.be/api/web-pages/hps/${name}`);
  }

  getAppointments({from, to, motiveId, isNewPatient, calendarId, state}: GetAppointmentsParams) {
    return this.http.get<Availability[]>('https://staging-api.rosa.be/api/availabilities', {
      params: {
        from,
        to,
        state,
        motive_id: motiveId,
        is_new_patient: isNewPatient,
        calendar_ids: calendarId,
      }
    });
  }
}

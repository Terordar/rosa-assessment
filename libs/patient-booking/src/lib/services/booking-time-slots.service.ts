import { Injectable } from '@angular/core';
import { map, Observable, startWith } from 'rxjs';
import { addMinutes, endOfDay, startOfDay } from 'date-fns';

import { BookingService } from './booking.service';
import { CalendarConfiguration, Doctor, Motive } from '../shared/doctor.model';
import { Availability } from '../shared/availability.model';

interface GetTimeSlotsParams {
  motiveId: Motive['id'];
  isNewPatient: boolean;
  doctorDetails: Doctor;
  days: Date[];
}

export interface TimeSlot {
  startTime: Date;
}

export interface TimeSlots {
  result: Map<string, TimeSlot[]> | null;
  maxRows: null[];
}

@Injectable({
  providedIn: 'root'
})
export class BookingTimeSlotsService {
  constructor(private bookingService: BookingService) {
  }

  getTimeSlots({motiveId, isNewPatient, doctorDetails, days}: GetTimeSlotsParams):Observable<TimeSlots> {
    const params = {
      motiveId,
      isNewPatient,
      calendarId: doctorDetails.calendars[0].id,
      state: 'open',
      from: startOfDay(new Date(days[0])).toISOString(),
      to: endOfDay(new Date(days[days.length - 1])).toISOString(),
    };

    return this.bookingService.getAppointments(params).pipe(
      map((data) => {
        const duration = doctorDetails.motives
          .find((motive: Motive) => motive.id === motiveId)
          ?.calendarConfigurations?.find((config: CalendarConfiguration) => config.calendarId === doctorDetails.calendars[0].id)
          ?.defaultDurations[isNewPatient ? 'newPatient' : 'existingPatient']?.duration;

        if (!duration) {
          return {
            maxRows: [],
            result: null,
          };
        }

        const daysMappedToSlots = days.reduce((acc, current) => {
          return acc.set(`${current.toDateString()}`, []);
        }, new Map<string, TimeSlot[]>());

        let maxPossibleRows = 0;

        data.forEach((availability: Availability) => {
          const possibleSlots = Math.floor(availability.duration / duration);

          maxPossibleRows = maxPossibleRows < possibleSlots ? possibleSlots : maxPossibleRows;

          const currentSlots = daysMappedToSlots.get(new Date(availability.startAt).toDateString()) ?? [];
          const slots = Array(possibleSlots).fill(null).map((_, i) => ({
            startTime: addMinutes(new Date(availability.startAt), i * duration)
          }));

          daysMappedToSlots.set(new Date(availability.startAt).toDateString(), [...currentSlots, ...slots]);
        });

        return {
          maxRows: Array(maxPossibleRows).fill(null),
          result: daysMappedToSlots,
        };
      }),
      startWith({result: null, maxRows: []}),
    );
  }
}

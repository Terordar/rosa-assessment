import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { addDays, subDays, isToday } from 'date-fns';
import { BehaviorSubject, combineLatest, map, Observable, shareReplay, switchMap, take } from 'rxjs';

import { BookingService } from '../../services/booking.service';
import { BookingTimeSlotsService, TimeSlot, TimeSlots } from '../../services/booking-time-slots.service';
import { Doctor } from '../../shared/doctor.model';

@Component({
  selector: 'rosa-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BookingComponent implements OnInit {
  nbOfDays = 7;
  maxDisplayedTimeSlots: number | null = 3;

  doctorDetails$ = new Observable<Doctor>();
  canCheckPreviousTimeSlots$ = new Observable<boolean>();
  timeSlots$ = new Observable<TimeSlots | null>();
  days$ = new BehaviorSubject(Array(this.nbOfDays).fill(new Date()).map((item, index) => {
    return addDays(item, index);
  }));

  selectedSlot: TimeSlot;

  bookingForm = this.fb.group({
    isNewPatient: [],
    motiveId: [''],
  });

  constructor(private bookingService: BookingService,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private titleService: Title,
              private bookingTimeSlotsService: BookingTimeSlotsService) {}

  // Required to keep dates ordered when iterating on the Map
  originalOrder() {
    return 0;
  }

  selectSlot(slot: TimeSlot) {
    this.selectedSlot = slot;
  }

  displayNextSlots() {
    this.days$.pipe(take(1)).subscribe(days => {
      this.days$.next(days.map(day => addDays(day, this.nbOfDays)))
    });
  }

  displayPreviousSlots() {
    this.days$.pipe(take(1)).subscribe(days => {
      this.days$.next(days.map(day => subDays(day, this.nbOfDays)))
    });
  }

  displayAllTimeSlots() {
    this.maxDisplayedTimeSlots = null;
  }

  ngOnInit() {
    this.doctorDetails$ = this.route.paramMap.pipe(
      switchMap(params => {
        const name = params.get('name');
        return this.bookingService.getDoctorByName(name ?? '');
      }),
      shareReplay()
    );

    this.doctorDetails$.pipe(take(1)).subscribe(
      ({ title, firstName, lastName }) => {
        this.titleService.setTitle(`Rosa: ${title} ${firstName} ${lastName}`);
      }
    );

    this.canCheckPreviousTimeSlots$ = this.days$.pipe(
      map(days => !isToday(days[0]))
    );

    this.timeSlots$ = combineLatest([
      this.bookingForm.controls['motiveId'].valueChanges,
      this.bookingForm.controls['isNewPatient'].valueChanges,
      this.doctorDetails$,
      this.days$
    ]).pipe(
      switchMap(([motiveId, isNewPatient, doctorDetails, days]) =>
        this.bookingTimeSlotsService.getTimeSlots({
          motiveId,
          isNewPatient,
          doctorDetails,
          days
        })
      ),
      shareReplay()
    );
  }
}
